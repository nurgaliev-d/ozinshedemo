
import UIKit

class TextField: UITextField {
    
    // MARK: UI Elements
    private lazy var iconImageView: UIImageView = UIImageView()
    
    // MARK: Properties
    @IBInspectable var iconImage: UIImage? {
        didSet {
            iconImageView.image = iconImage
        }
    }
    
    @IBInspectable var showTogglePasswordButton: Bool = false {
        didSet {
            if showTogglePasswordButton {
                setupTogglePasswordButton()
            }
        }
    }
    
    var valueChangeHandler: () -> Void = {}
    
    var insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 44, bottom: 0, right: 0)
    
    override var placeholder: String? {
        didSet {
            handlePlaceholderChange()
        }
    }
    
    init() {
        super.init(frame: .zero)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupUI()
    }
    
    func setupUI() {
        // Icon
        addSubview(iconImageView)
        
        iconImageView.tintColor = UIColor(named: "Colors/TextField/Icon")
        iconImageView.frame = CGRect(x: 16, y: 0, width: 20, height: bounds.height)
        iconImageView.contentMode = .center
        
        // Border
        layer.borderWidth = 1
        layer.borderColor = UIColor(named: "Colors/TextField/Border")?.cgColor
        layer.cornerRadius = 12
        
        // Color
        textColor = UIColor(named: "Colors/TextField/Text")
        
        // Font
        font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        // Placeholder
       handlePlaceholderChange()
        
        // Actions
        addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
    }
    
    func showErrorBorder() {
        layer.borderColor = UIColor(named: "Colors/Error")?.cgColor
    }
    
    func setupTogglePasswordButton() {
        
        insets.right = 52
        
        layoutSubviews()
        
        let togglePasswordButton = UIButton(type: .system)
        
        addSubview(togglePasswordButton)
        togglePasswordButton.frame = CGRect(x: bounds.width - 52, y: 0, width: 52, height: bounds.height)
        
        togglePasswordButton.setImage(UIImage(named: "Showpassword"), for: .normal)
        togglePasswordButton.tintColor = UIColor(named: "Colors/TextField/Icon")
        
        togglePasswordButton.addTarget(self, action: #selector(togglePassword), for: .touchUpInside)
    }
    
    func handlePlaceholderChange() {
        attributedPlaceholder = NSAttributedString(
            string: placeholder ?? "",
            attributes: [
                .foregroundColor: UIColor(named: "Colors/TextField/Placeholder")!,
                .font: UIFont.systemFont(ofSize: 16)
            ]
        )
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    // MARK: Methods
    
    @objc func editingDidBegin() {
        layer.borderColor = UIColor(named: "Colors/TextField/ActiveBorder")?.cgColor
        valueChangeHandler()
    }
    
    @objc func editingDidEnd() {
        layer.borderColor = UIColor(named: "Colors/TextField/Border")?.cgColor
    }
    
    @objc func togglePassword() {
        isSecureTextEntry.toggle()
    }
}
