//
//  SignInViewController.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 08.01.2024.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SignInViewController: UIViewController {
    
    @IBOutlet weak var passwordTextFIeld: TextFIeldWithPadding!
    @IBOutlet weak var emailTextField: TextFIeldWithPadding!
    @IBOutlet weak var signInButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    
    func configureViews(){
        emailTextField.layer.cornerRadius = 12.0
        emailTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.0).cgColor
        
        passwordTextFIeld.layer.cornerRadius = 12.0
        passwordTextFIeld.layer.borderWidth = 1.0
        passwordTextFIeld.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.0).cgColor
        
        signInButton.layer.cornerRadius = 12.0
    }
    
    func hideKeyboardWhenTappedAround(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func editingDidBegin(_ sender: TextFIeldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.59, green: 0.33, blue: 0.94, alpha: 1.00).cgColor
    }
    
    @IBAction func editingDidEnd(_ sender: TextFIeldWithPadding) {
        sender.layer.borderColor = UIColor(red: 0.90, green: 0.92, blue: 0.94, alpha: 1.00).cgColor
    }
    @IBAction func showPassword(_ sender: Any) {
        passwordTextFIeld.isSecureTextEntry = !passwordTextFIeld.isSecureTextEntry
    }
    
    
    @IBAction func signin(_ sender: Any) {
        let email = emailTextField.text!
        let password = passwordTextFIeld.text!
    
        if email.isEmpty || password.isEmpty {
            return
        }
    
    SVProgressHUD.show()
    
    let parameters = ["email": email,
                      "password": password]
    
    AF.request(Urls.SIGN_IN_URL, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseData { response in
        
        SVProgressHUD.dismiss()
        var resultString = ""
        if let data = response.data {
            resultString = String(data: data, encoding: .utf8)!
            print(resultString)
        }
        
        if response.response?.statusCode == 200 {
            let json = JSON(response.data!)
            print("JSON: \(json)")
            
            if let token = json["accessToken"].string {
                Storage.sharedInstance.accessToken = token
                UserDefaults.standard.set(token, forKey: "accessToken")
                self.startApp()
            } else {
                SVProgressHUD.showError(withStatus: "CONNECTION_ERROR".localized())
            }
        } else {
            var ErrorString = "CONNECTION_ERROR".localized()
            if let sCode = response.response?.statusCode {
                ErrorString = ErrorString + " \(sCode)"
            }
            ErrorString = ErrorString + " \(resultString)"
            SVProgressHUD.showError(withStatus: "\(ErrorString)")
        }
    }
}
    
    func startApp(){
        let tabViewController = self.storyboard?.instantiateViewController(identifier: "TabBarViewController")
        tabViewController?.modalPresentationStyle = .fullScreen
        self.present(tabViewController!, animated: true, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
