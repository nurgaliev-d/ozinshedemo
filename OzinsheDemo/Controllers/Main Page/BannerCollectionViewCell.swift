//
//  BannerCollectionViewCell.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 17.02.2024.
//

import UIKit
import SDWebImage

class BannerCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var bannerCtgView: UIView!
    @IBOutlet weak var ctgLabel: UILabel!
    @IBOutlet weak var titleeLabel: UILabel!
    @IBOutlet weak var bannnerImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    
        override func awakeFromNib() {        
            super.awakeFromNib()
            
            bannerCtgView.layer.cornerRadius = 8
        
            bannnerImageView.layer.cornerRadius = 12
    }
    
    func setData(bannerMovie: BannerMovie) {
        let transformer = SDImageResizingTransformer(size: CGSize(width: 300, height: 164), scaleMode: .aspectFill)
        
        bannnerImageView.sd_setImage(with: URL(string: bannerMovie.link), placeholderImage: nil, context: [.imageTransformer: transformer])
        
        if let categoryName = bannerMovie.movie.categories.first?.name {
            ctgLabel.text = categoryName
        }
        
        titleeLabel.text = bannerMovie.movie.name
        descLabel.text = bannerMovie.movie.description
    }
}
