//
//  ProfileViewController.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 27.10.2023.
//

import UIKit
import Localize_Swift

class ProfileViewController: UIViewController , LanguageProtocol{
    
    
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var switchModeLabel: UILabel!
    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var infosButton: UIButton!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var myProfileLabel: UILabel!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var languageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addView.accessibilityIdentifier = "View"
        updateInterfaceStyle()
    }
    
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        updateInterfaceStyle()
        
    }
    
    private func updateInterfaceStyle() {
        if darkModeSwitch.isOn {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .dark
                
            }
            addView.backgroundColor = .black
        } else {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
            }
            addView.backgroundColor = .white
        }
    }
        
        override func viewWillAppear(_ animated: Bool) {
            configureViews()
        }
        func configureViews(){
            myProfileLabel.text = "MY_PROFILE".localized()
            infosButton.setTitle("INFO".localized(), for: .normal)
            editLabel.text = "EDIT".localized()
            passwordButton.setTitle("PASSWORD".localized(), for: .normal)
            switchModeLabel.text = "SWITCH".localized()
            
            
            languageButton.setTitle("LANGUAGE".localized(), for: .normal)
            
            if Localize.currentLanguage() == "en"{
                languageLabel.text = "English"
            }
            if Localize.currentLanguage() == "kk"{
                languageLabel.text = "Қазақша"
            }
            if Localize.currentLanguage() == "ru"{
                languageLabel.text = "Русский"
            }
        }
        
        @IBAction func languageShow(_ sender: Any) {
            
            let languageVC = storyboard?.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
            
            languageVC.modalPresentationStyle = .overFullScreen
            
            languageVC.delegate = self
            
            present(languageVC, animated: true, completion: nil)
        }
        
        func languageDidChange() {
            configureViews()
        }
    
    @IBAction func logout(_ sender: Any) {
        let logoutVC = storyboard?.instantiateViewController(identifier: "LogOutViewController") as! LogOutViewController
        
        logoutVC.modalPresentationStyle = .overFullScreen
        
        present(logoutVC, animated: true, completion: nil)
        
    }
    
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
         }
         */
    }
