import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import Foundation

class ChangePasswordViewController: UIViewController {
    // MARK: IB Outlets
    @IBOutlet weak var passwordField0: TextField!
    @IBOutlet weak var passwordField: TextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var changeButton: UIButton!
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        hidePasswordError()
        passwordField0.valueChangeHandler = hidePasswordError
        changeButton.layer.cornerRadius = 12.0
    }
    
    func showPasswordError() {
        passwordErrorLabel.isHidden = false
        passwordErrorLabelTopConstraint.constant = 16
    }
    
    func hidePasswordError() {
        passwordErrorLabel.isHidden = true
        passwordErrorLabelTopConstraint.constant = 0
    }
    
    // MARK: Actions
    @IBAction func changeTapped(_ sender: Any) {
        guard let password0 = passwordField0.text?.trimmingCharacters(in: .whitespaces) else { return }
        guard let password = passwordField.text?.trimmingCharacters(in: .whitespaces) else { return }
        
        if password0.isEmpty || password.count < 6 {
            passwordField.showErrorBorder()
            return
        }
        
        if password.isEmpty || password.count < 6 {
            passwordField.showErrorBorder()
            return
        }
        
        let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        
        progressHUD.label.text = "Changing..."
    }
}
