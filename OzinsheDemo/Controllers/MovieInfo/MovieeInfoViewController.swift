//
//  MovieeInfoViewController.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 23.02.2024.
//

import UIKit
import SDWebImage
import SVProgressHUD
import SwiftyJSON
import Alamofire

class MovieeInfoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var posterImgView: UIImageView!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var bckgrView: UIView!
    @IBOutlet weak var detaillLabel: UILabel!
    @IBOutlet weak var nameLabell: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var dirLabel: UILabel!
    @IBOutlet weak var prodLabel: UILabel!
    @IBOutlet weak var fullDescButton: UIButton!
    @IBOutlet weak var descGrView: GradientView!
    @IBOutlet weak var seasonssLabel: UILabel!
    @IBOutlet weak var seasonssButton: UIButton!
    @IBOutlet weak var arrowImgView: UIImageView!
    @IBOutlet weak var scShotCollView: UICollectionView!
    @IBOutlet weak var similarCollView: UICollectionView!
    
    var movie = Movie()
    
    var similarMovies: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setData()
        configureViews()
        downloadSimilar()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func configureViews() {
        //backgroundView
        bckgrView.layer.cornerRadius = 32.0
        bckgrView.clipsToBounds = true
        bckgrView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        descLabel.numberOfLines = 4
        
        scShotCollView.delegate = self
        scShotCollView.dataSource = self
        
        if movie.movieType == "MOVIE" {
            seasonssLabel.isHidden = true
            seasonssButton.isHidden = true
            arrowImgView.isHidden = true
        }else {
            seasonssButton.setTitle("\(movie.seasonCount) сезон, \(movie.seriesCount) серия", for: .normal)
        }
        if descLabel.maxNumberOfLines < 5 {
            fullDescButton.isHidden = true
        }
        
        if movie.favorite {
            favButton.setImage(UIImage(named: "FavoriteSelected"), for: .normal)
        }else {
            favButton.setImage(UIImage(named: "favoriteButton"), for: .normal)
        }
    }
    func setData() {
        posterImgView.sd_setImage(with: URL(string: movie.poster_link), completed: nil)
        
        nameLabell.text = movie.name
        detaillLabel.text = "\(movie.year)"
        for item in movie.genres {
            detaillLabel.text = detaillLabel.text! + " • " + item.name
        }
        
        descLabel.text = movie.description
        dirLabel.text = movie.director
        prodLabel.text = movie.producer
    }
    func downloadSimilar() {
        SVProgressHUD.show()
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Storage.sharedInstance.accessToken)"
        ]
        
        AF.request(Urls.GET_SIMILAR + String(movie.id), method: .get, headers: headers).responseData { response in
            
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 {
                let json = JSON(response.data!)
                print("JSON: \(json)")
                
                if let array = json.array {
                    for item in array {
                        let movie = Movie(json: item)
                        self.similarMovies.append(movie)
                    }
                    self.similarCollView.reloadData()
                } else {
                    SVProgressHUD.showError(withStatus: "CONNECTION_ERROR".localized())
                }
            } else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
        }
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func playMovie(_ sender: Any) {
        if movie.movieType == "MOVIE" {
            let playerVC = storyboard?.instantiateViewController(withIdentifier: "MoviePlayerViewController") as! MoviePlayerViewController
            playerVC.video_link = movie.video_link
            navigationController?.show(playerVC, sender: self)
        }else {
            let seasonsVC = storyboard?.instantiateViewController(withIdentifier: "SeasonSeriesViewController") as! SeasonSeriesViewController
            seasonsVC.movie = movie
            navigationController?.show(seasonsVC, sender: self)
        }
    }
    
    @IBAction func addFavorite(_ sender: Any) {
        var method = HTTPMethod.post
        if movie.favorite {
            method = .delete
        }
        
        SVProgressHUD.show()
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(Storage.sharedInstance.accessToken)"
        ]
        
        let parameters = ["movieId": movie.id] as [String : Any]
        
        AF.request(Urls.FAVORITE_URL, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData { response in
            
            SVProgressHUD.dismiss()
            var resultString = ""
            if let data = response.data {
                resultString = String(data: data, encoding: .utf8)!
                print(resultString)
            }
            
            if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
//                let json = JSON(response.data!)
//                print("JSON: \(json)")
                
                self.movie.favorite.toggle()
                
                self.configureViews()
                
            } else {
                var ErrorString = "CONNECTION_ERROR".localized()
                if let sCode = response.response?.statusCode {
                    ErrorString = ErrorString + " \(sCode)"
                }
                ErrorString = ErrorString + " \(resultString)"
                SVProgressHUD.showError(withStatus: "\(ErrorString)")
            }
        }
    }
    
    @IBAction func shareMovie(_ sender: Any) {
        let text = "\(movie.name) \n\(movie.description)"
        let image = posterImgView.image
        let shareAll = [text, image!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func fullDescription(_ sender: Any) {
        if descLabel.numberOfLines > 4 {
            descLabel.numberOfLines = 4
            fullDescButton.setTitle("Толығырақ", for: .normal)
            descGrView.isHidden = false
        } else {
            descLabel.numberOfLines = 30
            fullDescButton.setTitle("Жасыру", for: .normal)
            descGrView.isHidden = true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - collectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.similarCollView {
            return similarMovies.count
        }
        return movie.screenshots.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.similarCollView {
            let similarCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            
            let transformer = SDImageResizingTransformer(size: CGSize(width: 112, height: 164), scaleMode: .aspectFill)
            
            let imageview = similarCell.viewWithTag(1000) as! UIImageView
            imageview.sd_setImage(with: URL(string: similarMovies[indexPath.row].poster_link), placeholderImage: nil, context: [.imageTransformer: transformer])
            imageview.layer.cornerRadius = 8
            
            //movieNameLabel
            let movieNameLabel = similarCell.viewWithTag(1001) as! UILabel
            movieNameLabel.text = similarMovies[indexPath.row].name
            
            let movieGenreNameLabel = similarCell.viewWithTag(1002) as! UILabel
            if let genrename = similarMovies[indexPath.row].genres.first {
                movieGenreNameLabel.text = genrename.name
            } else {
                movieGenreNameLabel.text = ""
            }
            
            return similarCell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        //imageview
        let transformer = SDImageResizingTransformer(size: CGSize(width: 184, height: 112), scaleMode: .aspectFill)
        
        let imageview = cell.viewWithTag(1000) as! UIImageView
        imageview.layer.cornerRadius = 8
        
        imageview.sd_setImage(with: URL(string: movie.screenshots[indexPath.row].link), placeholderImage: nil, context: [.imageTransformer: transformer])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.similarCollView {
            let movieinfoVC = storyboard?.instantiateViewController(withIdentifier: "MovieeInfoViewController") as! MovieeInfoViewController
            
            movieinfoVC.movie  = similarMovies[indexPath.row]
            
            navigationController?.show(movieinfoVC, sender: self)
        }
    }
}
