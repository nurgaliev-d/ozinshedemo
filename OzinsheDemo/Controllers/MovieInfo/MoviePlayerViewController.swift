//
//  MoviePlayerViewController.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 23.02.2024.
//

import UIKit
import YouTubePlayer
//import XCDYouTubeKit

class MoviePlayerViewController: UIViewController {
    @IBOutlet weak var player: YouTubePlayerView!
    
    var video_link = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        player.loadVideoID(video_link)
    }

}
