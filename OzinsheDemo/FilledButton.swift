//
//  FilledButton.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 09.11.2023.
//

import Foundation
import UIKit

class FilledButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    func setupUI() {
        // Background
        backgroundColor = UIColor(named: "Colors/Button/Fill")
        
        // Title
        setTitleColor(UIColor(named: "Colors/Button/Text"), for: .normal)
        
        // Font
        titleLabel?.font = .systemFont(ofSize: 16, weight: .semibold)
        
        // Border
        layer.cornerRadius = 12
    }
}
