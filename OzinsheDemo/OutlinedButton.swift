//
//  OutlinedButton.swift
//  OzinsheDemo
//
//  Created by Диас Нургалиев on 09.11.2023.
//

import Foundation
import UIKit

class OutlinedButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }
    
    func setupUI() {
        // Background
        backgroundColor = UIColor(named: "Colors/Button/Outlined/Fill")
        
        // Title
        setTitleColor(UIColor(named: "Colors/Button/Outlined/Text"), for: .normal)
        
        // Font
        titleLabel?.font = .systemFont(ofSize: 14, weight: .semibold)
        
        // Border
        layer.cornerRadius = 12
        layer.borderWidth = 1
        layer.borderColor = UIColor(named: "Colors/Button/Outlined/Border")?.cgColor
    }
}

